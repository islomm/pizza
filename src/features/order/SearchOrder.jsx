import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

const SearchOrder = () => {
  const [query, setQuery] = useState("");
  const navigate = useNavigate();

  function handleSubmit(e) {
    e.preventDefault();
    if (!query) return;
    navigate(`/order/${query}`);
    setQuery("");
  }
  return (
    <>
      <form onSubmit={handleSubmit}>
        <input
          className="w-[150px] md:w-[220px] rounded-full focus:outline-none bg-yellow-50 py-2 px-4  "
          placeholder="Search Order #"
          value={query}
          onChange={(e) => setQuery(e.target.value)}
        ></input>
      </form>
    </>
  );
};

export default SearchOrder;
