import React from "react";
import Button from "../../ui/Button";
import { useDispatch, useSelector } from "react-redux";
import { decreaseCart, getCurrentQuantity, increaseCart } from "./cartSlice";

const UpdateCartQty = ({ id, itemQuantity }) => {
  const dispatch = useDispatch();
  console.log(itemQuantity);

  return (
    <div className="flex items-center gap-2 md:gap-5">
      <Button type="rounded" onClick={() => dispatch(decreaseCart(id))}>
        -
      </Button>
      <span className="text-sm md:text-base">{itemQuantity.quantity}</span>
      <Button type="rounded" onClick={() => dispatch(increaseCart(id))}>
        +
      </Button>
    </div>
  );
};

export default UpdateCartQty;
